<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'eFone' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '0p-GD7wb#v>jEDUu*[y?/`LS^`>>gY8F:o1QjMX>1N|*(%=r+fc_P4IAU]M8nEa}' );
define( 'SECURE_AUTH_KEY',  'e Z(H],a|gNhGbl0m)&pbs/Aun%IJll</ KY aF;d=qK5o9uin]-q+&0?DHmFKGY' );
define( 'LOGGED_IN_KEY',    '#<gKpm]4>{R@~3`4{{ TSe~{T%du6Uw>ZPn0&P?%/B2A]uQoe|l98e[I}%T?P~-J' );
define( 'NONCE_KEY',        'p+=xw%9Ca8?2?w6W&5H{Q0{ebUKFKdNeHxKbvb1Rb+3vR9*?Q6&..uC]IRfkjij&' );
define( 'AUTH_SALT',        'yUdJ2/:q3CvJy{okA5,Qb,?m4X LVss8QK!R_0{I3Mo1*hrKYznGje/y6kTLbYTM' );
define( 'SECURE_AUTH_SALT', 'f6NP_]t[u`)r|ei&C_JsZ,>#cts6VaMwJY~3U{odNj64__Jr{tVb$we d|.>~ ic' );
define( 'LOGGED_IN_SALT',   '`DhGL3|:9wV1M[,)_^SK#,pe;M92DF@WP0?ap|,7sq9r^Ihc]ju7j~v#z]=NEg%E' );
define( 'NONCE_SALT',       '29yspTSN*d;=bi3`L2b&-sN=bp6)qTH~h<5|]grP =r:>YBL:>>g5-$(1v|Ps=+x' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
