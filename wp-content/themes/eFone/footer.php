<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

</div><!-- #content -->

<footer id="footer" class="footer" role="contentinfo">
	<div class="top_footer">
		<div class="container">
			<div class="clearfix">
				<div class="all">
					<h4>Our Products</h4>
					<ul class="list_item pad_right">
						<li><a href="#">eFone Mobile</a></li>
						<li><a href="#">eFone Landline</a></li>
						<li><a href="#">eFone Business</a></li>
					</ul>
				</div>
				<div class="all">
					<h4>eFone</h4>
					<ul class="list_item pad_right">
						<li><a href="#">Support</a></li>
						<li><a href="#">About Us</a></li>
						<li><a href="#">Blog</a></li>
					</ul>
				</div>
				<div class="all">
					<h4>Our Products</h4>
					<ul class="list_item">
						<li><a href="tal:18002342344">Customer Support : 1800 234 2344</a></li>
						<li><a href="tal:18003452345">Product Support : 1800 345 2345</a></li>
					</ul>
					<ul class="social">
						<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="bott_footer">
		<div class="container">
			<div class="clearfix">
				<div class="left">
					<p>&copy; 2019 by eFone</p>
				</div>
				<div class="right">
					<p>All Rights Reserved</p>
				</div>
			</div>
		</div>
	</div>
</footer>
</div><!-- .site-content-contain -->
</div><!-- #page -->
<?php wp_footer(); ?>

</body>

</html>