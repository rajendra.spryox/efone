<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<div id="page" class="site">
		<header id="header" class="header">
			<div class="header_container">
				<div class="clearfix">
					<div class="logo">
						<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="Logo" width="HERE" height="HERE" />
						</a>
					</div>
					<div class="right_menu">
						<div class="toggle_menu">
							<div id="nav-icon2" class="">
								<span></span>
								<span></span>
								<span></span>
								<span></span>
								<span></span>
								<span></span>
							</div>
						</div>
						<?php if (has_nav_menu('top')) : ?>
							<div class="navigation-top">
								<div class="wrap">
									<?php get_template_part('template-parts/navigation/navigation', 'top'); ?>
								</div><!-- .wrap -->
							</div><!-- .navigation-top -->
						<?php endif; ?>
					</div>
				</div>
			</div>
		</header><!-- #masthead -->

		<?php

		/*
	 * If a regular post or page, and not the front page, show the featured image.
	 * Using get_queried_object_id() here since the $post global may not be set before a call to the_post().
	 */
		if ((is_single() || (is_page() && !twentyseventeen_is_frontpage())) && has_post_thumbnail(get_queried_object_id())) :
			echo '<div class="single-featured-image-header">';
			echo get_the_post_thumbnail(get_queried_object_id(), 'twentyseventeen-featured-image');
			echo '</div><!-- .single-featured-image-header -->';
		endif;
		?>

		<div class="site-content-contain">
			<div id="content" class="container">